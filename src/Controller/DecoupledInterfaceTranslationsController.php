<?php
/**
 * @file
 * Contains \Drupal\decoupled_interface_translations\Controller\DecoupledInterfaceTranslationsController.
 */
namespace Drupal\decoupled_interface_translations\Controller;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\locale\SourceString;
use Drupal\locale\StringStorageInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DecoupledInterfaceTranslationsController
 * @package Drupal\decoupled_interface_translations\Controller
 */
class DecoupledInterfaceTranslationsController extends ControllerBase implements ContainerInjectionInterface {

  const DECOUPLED_STRING_CONTEXT = 'Decoupled Translation';

  /**
   * @var StringStorageInterface
   */
  protected $localeStorage;
  /**
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * DecoupledInterfaceTranslationsController constructor.
   * @param StringStorageInterface $locale_storage
   * @param LanguageManagerInterface $language_manager
   */
  public function __construct(StringStorageInterface $locale_storage, LanguageManagerInterface $language_manager) {
    $this->localeStorage = $locale_storage;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('locale.storage'),
      $container->get('language_manager')
    );
  }

  /**
   * @return JsonResponse
   */
  public function get() {
    // We'll use this to build our interface translation json
    $translations = [];

    // Get enabled languages
    $enabled_languages = $this->languageManager->getLanguages();
    // For each enabled language
    foreach ($enabled_languages as $enabled_language) {
      $enabled_language_langcode = $enabled_language->getId();
      // Fetch the string translations
      $conditions = [
        'language' => $enabled_language_langcode,
        'translated' => true,
        // Only retrieve "Decoupled Translation" strings
        'context' => self::DECOUPLED_STRING_CONTEXT
      ];
      $interface_translations = $this->localeStorage->getTranslations($conditions);
      // For each translation
      foreach ($interface_translations as $interface_translation) {
        // Add the translation to our array
        $translations[$enabled_language_langcode][$interface_translation->source] = $interface_translation->translation;
      }
    }

    return new JsonResponse($translations);
  }

  /**
   * @param Request $request
   * @return JsonResponse
   * @throws \Drupal\locale\StringStorageException
   */
  public function add(Request $request) {
    $source_strings = json_decode($request->getContent());
    $created_source_strings = [];
    $existing_source_strings = [];

    foreach ($source_strings as $source_string) {
      // Find existing source string.

      $storage = \Drupal::service('locale.storage');

      $string = $storage->findString(array('source' => $source_string, 'context' => self::DECOUPLED_STRING_CONTEXT));
      // If the source string doesn't exist, add it.
      if (is_null($string)) {
        $string = new SourceString();
        $string->setString($source_string);
        // Tag the string with the 'Decoupled Translation' context.
        $string->setValues(['context' => self::DECOUPLED_STRING_CONTEXT]);
        $string->setStorage($storage);
        $string->save();
        $created_source_strings[] = ['lid' => $string->getId(), 'source_string' => $string->getString()];
      } else {
        // No need to do anything, this string has already been added to Drupal.
        $existing_source_strings[] = ['lid' => $string->getId(), 'source_string' => $string->getString()];
      }
    }

    $message_template = $this->t('Created %d new source string(s), and found %d existing source string(s).');
    return new JsonResponse(['message' => sprintf($message_template, count($created_source_strings), count($existing_source_strings)),'created'=>$created_source_strings, 'existing' => $existing_source_strings]);
  }
}
